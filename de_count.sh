#!/bin/bash
#
# Description: count frequency of word 'de'
#
# Usage: ./de_count.sh FILE

FILE=$1
grep -Eo '\w+' $FILE | \
grep -wic 'de'
